module Vagas
  module Conference
    class Base
      attr_accessor :tracks

      def initialize(filename)
        @filename = filename
        @tracks = []
      end

      def self.plan(filename)
        self.new(filename).plan
      end

      def plan
        raise Vagas::NonexistentFileException.new("The file: #{@filename} does not exist") unless file_exist?

        @tracks = Generator.generate_tracks(file)

        print
      end

      def print
        tracks[0..1].each_with_index do |track, index|
          puts "\nTrack #{index + 1}"
          track.print
        end

        nil
      end

      private
      def file_exist?
        File.exist?(@filename)
      end

      def file
        @file ||= File.new(@filename, "r")
      end
    end
  end
end
