module Vagas
  module Conference
    class Session
      attr_accessor :talks, :start_time

      def initialize(talks, start_time)
        @talks = talks
        @start_time = start_time
      end

      def print
        time = start_time.dup

        talks.each do |talk|
          talk.print(time)

          time += (talk.duration * 60)
        end

        time
      end
    end
  end
end
