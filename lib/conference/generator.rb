module Vagas
  module Conference
    class Generator
      attr_reader :tracks

      def initialize(file)
        @file = file
      end

      def self.generate_tracks(file)
        self.new(file).generate_tracks
      end

      def generate_tracks
        @talks = FileParser.parse!(@file)

        build_tracks

        @tracks
      end

      private
      def build_tracks
        @tracks = []

        while @talks.any? do
          track, @talks = Track.build(@talks)

          @tracks.push track
        end
      end
    end
  end
end
