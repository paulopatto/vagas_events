module Vagas
  module Conference
    class Track
      attr_accessor :morning_session, :afternoon_session

      def initialize(talks)
        @backlog_talks = talks
      end

      def self.build(talks)
        self.new(talks).build
      end

      def build
        build_morning_session
        build_afternoon_session

        [self, @backlog_talks]
      end

      def build_morning_session
        now = Time.now
        time = Time.new(now.year, now.month, now.day, 9, 0, 0)
        
        talks = pick_talks

        @morning_session = Session.new(talks, time)
      end

      def build_afternoon_session
        now = Time.now
        time = Time.new(now.year, now.month, now.day, 13, 0, 0)

        talks = pick_talks(240)

        @afternoon_session = Session.new(talks, time)
      end

      def print
        morning_session.print
        print_lunch_break
        end_time = afternoon_session.print
        print_networking_event(end_time)
      end

      private
      def pick_talks(max_duration=180, min_duration=180)
        result = []
        duration = 0

        while check_duration(duration, max_duration, min_duration)
          duration = select_talks(result, duration, max_duration)

          break unless duration
        end

        result
      end

      def check_duration(duration, max_duration, min_duration)
        duration < max_duration || (duration < max_duration && duration >= min_duration)
      end

      def select_talks(result, duration, max_duration)
        duration_left = max_duration - duration

        candidate_talk = @backlog_talks.find { |talk| talk.duration <= duration_left }

        return add_talk(candidate_talk, result, duration) if candidate_talk

        false
      end

      def add_talk(talk, result, duration)
        @backlog_talks -= [talk]

        duration += talk.duration

        result.push talk

        duration
      end

      def print_lunch_break
        puts "12:00PM Lunch"
      end

      def print_networking_event(time)
        puts "#{time.strftime("%I:%M%p")} Networking Event"
      end
    end
  end
end
