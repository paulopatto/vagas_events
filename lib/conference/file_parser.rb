module Vagas
  module Conference
    class FileParser
      def initialize(file)
        @file = file
      end

      def self.parse!(file)
        self.new(file).parse!
      end

      def parse!
        result = []

        @file.each do |line|
          duration = line.split(' ').last
          title = line.gsub(duration, '')

          duration = duration.gsub('\n', '')

          result.push build_talk(title, duration) if title.size > 0 && duration.size > 0
        end

        result
      rescue NoMethodError
        raise Vagas::InvalidFileException.new('Given file is in invalid format')
      end

      def build_talk(title, duration)
        Talk.new(title, duration.strip)
      end
    end
  end
end
