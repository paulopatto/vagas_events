module Vagas
  module Conference
    class Talk
      attr_accessor :title, :duration

      def initialize(title, duration)
        @title = title
        @duration = convert_duration(duration)
      end

      def print(time)
        puts "#{time.strftime("%I:%M%p")} #{title}"
      end

      private
      def convert_duration(duration)
        duration = duration_codes[duration] || duration.to_i if duration.is_a?(String)

        raise Vagas::InvalidFileException.new('Given file is in invalid format') if duration.nil? || duration == 0

        duration
      end

      def duration_codes
        @duration_codes ||= { 'lightning' => 5 }
      end
    end
  end
end
