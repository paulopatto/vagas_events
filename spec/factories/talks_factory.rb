class TalksFactory
  def self.get
    [
      Vagas::Conference::Talk.new("Writing Fast Tests Against Enterprise Rails", 60),
      Vagas::Conference::Talk.new("Overdoing it in Python", 45),
      Vagas::Conference::Talk.new("Lua for the Masses", 30),
      Vagas::Conference::Talk.new("Ruby Errors from Mismatched Gem Versions", 45),
      Vagas::Conference::Talk.new("Common Ruby Errors", 45),
      Vagas::Conference::Talk.new("Rails for Python Developers", 5),
      Vagas::Conference::Talk.new("Communicating Over Distance", 60),
      Vagas::Conference::Talk.new("Accounting-Driven Development", 45),
      Vagas::Conference::Talk.new("Woah", 30),
      Vagas::Conference::Talk.new("Sit Down and Write", 30),
      Vagas::Conference::Talk.new("Pair, Programming vs Noise", 45),
      Vagas::Conference::Talk.new("Rails Magic", 60),
      Vagas::Conference::Talk.new("Ruby on Rails: Why We Should Move On", 60),
      Vagas::Conference::Talk.new("Clojure Ate Scala (on my project)", 45),
      Vagas::Conference::Talk.new("Programming in the Boondocks of Seattle", 30),
      Vagas::Conference::Talk.new("Ruby vs. Clojure for Back-End Development", 30),
      Vagas::Conference::Talk.new("Ruby on Rails Legacy App Maintenance", 60),
      Vagas::Conference::Talk.new("A World Without HackerNews", 30),
      Vagas::Conference::Talk.new("User Interface CSS in Rails Apps", 30)
    ]
  end
end
