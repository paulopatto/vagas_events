describe Vagas::Conference::Base do
  describe '.plan' do
    context 'when filename is passed' do
      let(:filename) { 'filename' }
      let(:instance) { described_class.new(filename) }

      subject { described_class.plan(filename) }

      before do
        allow(described_class).to receive(:new).and_return(instance)
      end

      it 'calls #plan' do
        expect(instance).to receive(:plan)

        subject
      end
    end

    context 'when filename is not passed' do
      subject { described_class.plan }

      it 'raises an ArgumentError' do
        expect{subject}.to raise_error(ArgumentError)
      end
    end
  end

  describe '#plan' do
    subject { described_class.new(filename).plan }

    context 'when file exists' do
      context 'and is valid' do
        let(:filename) { "#{Dir.pwd}/spec/support/valid_file" }
        let(:file) { File.new(filename) }

        before do
          allow(File).to receive(:new).and_return(file)
        end
        
        it 'sends file to generator class' do
          expect(Vagas::Conference::Generator).to receive(:generate_tracks).with(file).and_return([])

          subject
        end
      end

      context 'and is not valid' do
        let(:filename) { "#{Dir.pwd}/spec/support/invalid_file" }
      end
    end

    context 'when file does not exist' do
      let(:filename) { "#{Dir.pwd}/spec/support/nonexistent_file" }

      it 'raises a NonexistentFileException' do
        expect{subject}.to raise_error(Vagas::NonexistentFileException)
      end
    end
  end

  describe '#print' do
    let(:filename) { "#{Dir.pwd}/spec/support/valid_file" }
    let(:instance) { described_class.new(filename) }
    let(:track) { double }

    before do
      instance.tracks = [track, track]
    end

    subject { instance.print }
    
    it 'calls print on all tracks' do
      expect(track).to receive(:print).twice

      subject
    end
  end
end