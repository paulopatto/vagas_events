describe Vagas::Conference::Talk do
  describe '.initialize' do
    context 'when duration is nil' do
      subject { described_class.new('title', nil) }

      it 'raises InvalidFileException' do
        expect{subject}.to raise_error(Vagas::InvalidFileException)
      end
    end

    context 'when duration id an int' do
      subject { described_class.new('title', 10) }

      it 'sets title' do
        expect(subject.title).to eq 'title'
      end

      it 'sets duration' do
        expect(subject.duration).to eq 10
      end
    end

    context 'when duration is string' do
      context 'and string is valid' do
        subject { described_class.new('title', 'lightning') }

        it 'sets title' do
          expect(subject.title).to eq 'title'
        end

        it 'sets duration' do
          expect(subject.duration).to eq 5
        end
      end

      context 'and string is invalid' do
        subject { described_class.new('title', 'thunder') }

        it 'raises InvalidFileException' do
          expect{subject}.to raise_error(Vagas::InvalidFileException)
        end
      end
    end
  end

  describe '#print' do
    let(:time) { Time.new(2015, 7, 1, 9, 0, 0) }
    subject { described_class.new('title', 60).print(time) }
    
    it 'outputs correct message' do
      expect{subject}.to output("09:00AM title\n").to_stdout
    end
  end
end
