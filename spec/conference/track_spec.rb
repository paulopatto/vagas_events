describe Vagas::Conference::Track do
  let(:talks) { [Vagas::Conference::Talk.new('talk1', 60), Vagas::Conference::Talk.new('talk2', 240), Vagas::Conference::Talk.new('talk3', 60), Vagas::Conference::Talk.new('talk4', 60)] }

  describe '.build' do
    let(:instance) { described_class.new(talks) }

    before do
      allow(described_class).to receive(:new).and_return(instance)
    end

    subject { described_class.build(talks) }

    it 'calls #build' do
      expect(instance).to receive(:build)

      subject
    end
  end

  describe '#build' do
    subject { described_class.new(talks).build }

    it 'creates morning session' do
      expect(subject.first.morning_session).not_to be_nil
    end

    it 'creates afternoon session' do
      expect(subject.first.afternoon_session).not_to be_nil
    end
  end

  describe '#build_morning_session' do
    let(:session_talks) { [talks[0], talks[2], talks[3]] }
    let(:time) { Time.now }

    before do
      allow(Time).to receive(:new).with(time.year, time.month, time.day, 9, 0, 0).and_return(time)
    end

    subject { described_class.new(talks).build_morning_session }

    it 'creates morning section' do
      expect(Vagas::Conference::Session).to receive(:new).with(session_talks, time)

      subject
    end
  end

  describe '#build_afternoon_session' do
    let(:session_talks) { [talks[0], talks[2], talks[3]] }
    let(:time) { Time.now }

    before do
      allow(Time).to receive(:new).with(time.year, time.month, time.day, 13, 0, 0).and_return(time)
    end

    subject { described_class.new(talks).build_afternoon_session }

    context 'when talks fits the tracks time' do
      it 'creates afternoon section' do
        expect(Vagas::Conference::Session).to receive(:new).with(session_talks, time)

        subject
      end
    end

    context 'when talks do not fit the tracks time' do
      let(:talks) { [Vagas::Conference::Talk.new('talk2', 200), Vagas::Conference::Talk.new('talk1', 60), Vagas::Conference::Talk.new('talk3', 60), Vagas::Conference::Talk.new('talk4', 60)] }
      let(:session_talks) { [talks[0]] }

      subject { described_class.new(talks).build_afternoon_session }

      it 'creates afternoon section' do
        expect(Vagas::Conference::Session).to receive(:new).with(session_talks, time)

        subject
      end

      it 'adds 1 talks to track' do
        expect(subject.talks.size).to eq(1)
      end
    end
  end

  describe '#print' do
    let(:instance) { described_class.new(talks) }
    let(:session) { double }

    before do
      allow(session).to receive(:print).and_return(Time.now)
      allow(instance).to receive(:morning_session).and_return(session)
      allow(instance).to receive(:afternoon_session).and_return(session)
    end

    subject { instance.print }

    it 'calls #print on sessions' do
      expect(session).to receive(:print).twice

      subject
    end

    it 'prints lunch break at noon' do
      expect(instance).to receive(:print_lunch_break)

      subject
    end

    it 'prints networking event' do
      expect(instance).to receive(:print_networking_event)

      subject
    end
  end
end
