describe Vagas::Conference::Session do
  describe '.initialize' do
    let(:talks) { [1,2,3,4] }
    subject { described_class.new(talks, double) }

    it 'sets talks' do
      expect(subject.talks).to eq talks
    end
  end

  describe '#print' do
    let(:talk) { Vagas::Conference::Talk.new('title', 60) }
    let(:time) { Time.now }
    let(:talks) { [talk,talk,talk,talk] }

    subject { described_class.new(talks, time).print }

    it 'print talks' do
      expect(talk).to receive(:print).exactly(4).times

      subject
    end
  end
end
