require './spec/factories/talks_factory'

describe Vagas::Conference::Generator do
  describe '.generate_tracks' do
    let(:file) { File.new("#{Dir.pwd}/spec/support/valid_file") }

    context 'when file is passed' do
      let(:instance) { described_class.new(file) }

      subject { described_class.generate_tracks(file) }

      before do
        allow(described_class).to receive(:new).and_return(instance)
      end

      it 'calls #generate_tracks' do
        expect(instance).to receive(:generate_tracks)

        subject
      end

      it 'creates 3 tracks' do
        expect(subject.size).to eq 3
      end
    end

    context 'when file is not passed' do
      subject { described_class.generate_tracks }

      it 'raises an ArgumentError' do
        expect{subject}.to raise_error(ArgumentError)
      end
    end
  end

  describe '#generate_tracks' do
    subject { described_class.new(file).generate_tracks }

    context 'when file is valid' do
      let(:filename) { "#{Dir.pwd}/spec/support/valid_file" }
      let(:file) { File.new(filename) }
      let(:list_of_talks) { TalksFactory.get }

      before do
        allow(File).to receive(:new).and_return(file)
        allow(Vagas::Conference::FileParser).to receive(:parse!).and_return(list_of_talks)
      end

      it 'parses the file' do
        expect(Vagas::Conference::FileParser).to receive(:parse!).with(file)

        subject
      end
    end

    context 'when file is not valid' do
      let(:filename) { "#{Dir.pwd}/spec/support/invalid_file" }
    end
  end
end
