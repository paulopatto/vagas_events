describe Vagas::Conference::FileParser do
  describe '.parse!' do
    let(:file) { File.new("#{Dir.pwd}/spec/support/valid_file") }
    let(:instance) { described_class.new(file) }

    subject { described_class.parse!(file) }

    before do
      allow(described_class).to receive(:new).and_return(instance)
    end

    it 'calls #parse!' do
      expect(instance).to receive(:parse!)

      subject
    end
  end

  describe '#parse!' do
    subject { described_class.new(file).parse! }

    context 'when file is valid' do
      let(:file) { File.new("#{Dir.pwd}/spec/support/valid_file") }

      before do
        allow(File).to receive(:new).and_return(file)
      end
      
      it 'creates Talks' do
        expect(subject.first).to be_an(Vagas::Conference::Talk)
      end

      it 'creates 22 talks' do
        expect(subject.size).to eq 22
      end
    end

    context 'and is not valid' do
      let(:file) { File.new("#{Dir.pwd}/spec/support/invalid_file") }

      it 'raises InvalidFileException' do
        expect{subject}.to raise_error(Vagas::InvalidFileException)
      end
    end
  end

  describe '#build_talk' do
    let(:instance) { described_class.new(File.new("#{Dir.pwd}/spec/support/valid_file")) }
    subject { instance.build_talk('title', ' 60') }

    it 'converts duration to int' do
      expect(subject.duration).to eq(60)
    end
  end
end
