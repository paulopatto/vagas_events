## Design

Eu procurei separar ao máximo o projeto em classes especializadas, separando as responsibilidades e facilitando tanto no entendimento como em possíveis futuras reestruturações do código.

Porém acabei ficando um pouco sem tempo e algumas features que deveriam ter uma classe especializada (como a impressão do resultado na tela) acabaram ficando espalhadas nas classses



## Instruções para executar

Para rodar o projeto abra o console do ruby a partir do diretorio do projeto e execute os seguintes comandos:

```ruby
require './lib/vagas'
file = File.new "#{Dir.pwd}/spec/support/valid_file" # Ou o caminho de outro arquivo
c = Vagas::Conference::Base.new file
c.plan
```